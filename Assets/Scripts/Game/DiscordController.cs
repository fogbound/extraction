﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteAlways]
public class DiscordController : MonoBehaviour
{
    public DiscordRpc.RichPresence presence = new DiscordRpc.RichPresence();
    public string applicationId = "416994065025073155"; // Unity app id
    public string steamId;
    DiscordRpc.EventHandlers handlers;

    public bool shouldUpdatePresence = true;
    public float updateTime = 5f;
    private float lastUpdateTime = 0f;

    private long startTimestamp;
    private string pDetails;
    [SerializeField] private string largeImgKey = "unity_logo";

    public void ReadyCallback (ref DiscordRpc.DiscordUser user) {
        Debug.Log($"Discord: connected to {user.username}#{user.discriminator} ({user.userId}).");
    }

    private void Update () {
        DiscordRpc.RunCallbacks();

        if (lastUpdateTime > updateTime) {
            UpdatePresence();
        }
        lastUpdateTime += Time.deltaTime;
    }

    private void UpdatePresence () {
        if (!shouldUpdatePresence) return;

        string appName = Application.productName;
        if (EditorApplication.isPlaying) pDetails = $"Playing {appName}";
        else if (EditorApplication.isCompiling) pDetails = $"Compiling scripts";
        else pDetails = $"Working on {appName}";

        presence.details = pDetails;
        presence.startTimestamp = startTimestamp;
        presence.largeImageKey = largeImgKey;
        presence.largeImageText = $"{Application.unityVersion} {Application.platform.ToString()}";

        DiscordRpc.UpdatePresence(presence);
        lastUpdateTime = 0f;
    }

    private void OnEnable () {
        Debug.Log("Discord: init");
        handlers = new DiscordRpc.EventHandlers();
        handlers.readyCallback += ReadyCallback;
        DiscordRpc.Initialize(applicationId, ref handlers, true, steamId);

        startTimestamp = System.DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
    }

    private void OnDisable () {
        Debug.Log("Discord: shutdown");
        DiscordRpc.Shutdown();
    }
}
