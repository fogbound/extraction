﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class AchievementManager : MonoBehaviour
{

    public static AchievementManager Instance { get; private set; }
    
    public enum Achievements : int {
        ACH_OPEN_GAME
    }

    private void Awake () {
        if (!Instance) Instance = this;
        else Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public void UnlockAchievement (Achievements ach) {
        if (!SteamManager.Initialized) return;
        string achName = System.Enum.GetName(typeof(Achievements), ach);
        bool achUnlocked = false;
        if (SteamUserStats.GetAchievement(achName, out achUnlocked) && achUnlocked) {
            Debug.LogWarning($"{SteamFriends.GetPersonaName()} already unlocked achievement {achName}.");
        }
        SteamUserStats.SetAchievement(achName);
        Debug.Log($"{SteamFriends.GetPersonaName()} unlocked achievement {achName}!");
    }

    public void ClearAchievements () {
        foreach (string ach in System.Enum.GetNames(typeof(Achievements)))
            SteamUserStats.ClearAchievement(ach);
    }

}
