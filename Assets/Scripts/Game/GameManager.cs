﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Steamworks;

public class GameManager : MonoBehaviour {

    public static GameManager Instance { get; private set; }

    [Header("Loading Screen")]
    [SerializeField] private GameObject LoadingScreen;
    [SerializeField] private Text LoadingScreenText;

    [Header("Scene Management")]
    [SerializeField] private string MainMenuSceneName;
    [SerializeField] private Scene activeScene;
    
    // Managers
    private AchievementManager m_AchManager;

    private void Awake () {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);

        if (SteamManager.Initialized) {
            Debug.Log($"Logged in to Steam as {SteamFriends.GetPersonaName()} ({SteamUser.GetSteamID()})!");
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start () {

        // Register managers
        m_AchManager = AchievementManager.Instance;

        // Load first scene (usually main menu)
        HideLoadingScreen();
        ChangeScene(MainMenuSceneName);

        // Unlock open game achievement
        m_AchManager.UnlockAchievement(AchievementManager.Achievements.ACH_OPEN_GAME);
    }

    #region Loading Screen Functions
    public void ShowLoadingScreen (string LoadingMessage) {
        LoadingScreenText.text = LoadingMessage;
        LoadingScreen.SetActive(true);
    }

    public void HideLoadingScreen () {
        LoadingScreenText.text = "";
        LoadingScreen.SetActive(false);
    }
    #endregion

    #region Scene Management Functions
    public void ChangeScene (string SceneName) {
        ShowLoadingScreen("Please wait...");
        if (activeScene.IsValid()) SceneManager.UnloadSceneAsync(activeScene);
        activeScene = SceneManager.GetSceneByName(SceneName);
        SceneManager.LoadScene(SceneName, LoadSceneMode.Additive);
        HideLoadingScreen();
    }
    #endregion

    #region Main Game Functions
    public void QuitGame () {
        Application.Quit();
    }
    #endregion

    #region Helper Functions
    public static Texture2D GetSteamImageAsTexture2D (int image) {
        Texture2D ret = null;
        uint imgWidth;
        uint imgHeight;
        bool bIsValid = SteamUtils.GetImageSize(image, out imgWidth, out imgHeight);

        if (bIsValid) {
            byte[] img = new byte[imgWidth * imgHeight * 4];
            bIsValid = SteamUtils.GetImageRGBA(image, img, (int)(imgWidth * imgHeight * 4));
            if (bIsValid) {
                ret = new Texture2D((int)imgWidth, (int)imgHeight, TextureFormat.RGBA32, false, true);
                ret.LoadRawTextureData(img);
                ret.Apply();
            }
        }

        if (ret != null) {
            Texture2D flipped = new Texture2D(ret.width, ret.height);
            int flX = ret.width;
            int flY = ret.height;

            for (int i = 0; i < flX; i++)
                for (int j = 0; j < flY; j++)
                    flipped.SetPixel(i, flY - j - 1, ret.GetPixel(i, j));
            flipped.Apply();
            ret = flipped;
        }

        return ret;
    }
    #endregion

}
