﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;

public class MainMenuController : MonoBehaviour
{

    private GameManager gm;
    private Timer uiUpdateTimer;

    [SerializeField] [Range(500f, 10000f)] double uiUpdateInterval;

    [Header("Menu Components")]
    [SerializeField] private Text steamUsernameLabel;
    [SerializeField] private RawImage steamUserAvatarImage;

    [Header("Steam User Info")]
    [SerializeField] string steamUsername;
    [SerializeField] string steamUserId;

    private void Start () {
        gm = GameManager.Instance;
        uiUpdateTimer = new Timer();
        uiUpdateTimer.Interval = uiUpdateInterval;
        uiUpdateTimer.Elapsed += UpdateUI;
        uiUpdateTimer.Start();

        UpdateUI();
    }

    private void OnApplicationQuit () {
        uiUpdateTimer.Stop();
    }

    private void GetSteamInfo () {
        steamUsername = SteamFriends.GetPersonaName();
        steamUserId = SteamUser.GetSteamID().ToString();
    }

    private void UpdateUI (object sender = null, ElapsedEventArgs e = null) {
        Debug.Log("Running UpdateUI");
        // Update local user info
        GetSteamInfo();
        steamUsernameLabel.text = steamUsername;
        steamUserAvatarImage.texture = GameManager.GetSteamImageAsTexture2D(
            SteamFriends.GetMediumFriendAvatar(SteamUser.GetSteamID()));
    }

    public void LoadScene (string SceneName) {
        gm.ChangeScene(SceneName);
    }

}
