﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using Steamworks;

[System.Serializable]
public class UNETServerController {
    public string autoInviteSteamId;
    public GameObject playerPrefab;

    private List<NetworkConnection> connectedClients = new List<NetworkConnection>();

    private Callback<P2PSessionRequest_t> m_P2PSessionRequested;

    [HideInInspector] public bool inviteFriendOnStart = false;

    public NetworkClient myClient {
        get { return SteamNetworkManager.Instance.myClient; }
        set { SteamNetworkManager.Instance.myClient = value; }
    }

    public CSteamID SteamLobbyID {
        get { return SteamNetworkManager.Instance.steamLobbyId; }
    }

    public void Init () {
        if (SteamManager.Initialized) {
            m_P2PSessionRequested = Callback<P2PSessionRequest_t>.Create(OnP2PSessionRequested);
        }
    }

    public void StartUNETServer () {
        if (SteamNetworkManager.Instance.lobbyConnectionState != SteamNetworkManager.SessionConnectionState.CONNECTED) {
            Debug.LogError("Not connected to lobby, exiting...");
            return;
        }

        Debug.Log("Starting UNET Server...");
        NetworkServer.RegisterHandler(NetworkMessages.SpawnRequestMsg, OnSpawnRequested);

        NetworkServer.Configure(SteamNetworkManager.hostTopology);
        NetworkServer.dontListen = true;
        NetworkServer.Listen(0);

        myClient = ClientScene.ConnectLocalServer();
        myClient.Configure(SteamNetworkManager.hostTopology);
        myClient.Connect("localhost", 0);
        myClient.connection.ForceInitialize();

        var serverToClientConn = NetworkServer.connections[0];
        AddConnection(serverToClientConn);

        SteamNetworkManager.Instance.RegisterNetworkPrefabs();

        ClientScene.Ready(serverToClientConn);
        SpawnPlayer(serverToClientConn);

        if (inviteFriendOnStart)
            SteamNetworkManager.Instance.StartCoroutine(DoShowInviteDialogWhenReady());
    }

    IEnumerator DoShowInviteDialogWhenReady () {
        Debug.Log("Waiting for UNET Server to start...");
        while (!NetworkServer.active)
            yield return null;

        Debug.Log("UNET Server started!");
        InviteFriendsToLobby();
    }

    public void InviteFriendsToLobby () {
        if (!string.IsNullOrEmpty(autoInviteSteamId.Trim())) {
            Debug.Log("Sending invite...");
            SteamFriends.InviteUserToGame(new CSteamID(ulong.Parse(autoInviteSteamId)), $"+connect_lobby {SteamLobbyID.m_SteamID.ToString()}");
        }
        else {
            Debug.Log("Showing invite friend dialog...");
            SteamFriends.ActivateGameOverlayInviteDialog(SteamLobbyID);
        }
    }

    bool SpawnPlayer (NetworkConnection conn) {
        NetworkServer.SetClientReady(conn);
        var player = GameObject.Instantiate(playerPrefab);
        return NetworkServer.SpawnWithClientAuthority(player, conn);
    }

    void DestroyPlayer (NetworkConnection conn) {
        if (conn == null) return;

        var objs = GameObject.FindObjectsOfType<NetworkIdentity>();
        for (int i = 0; i < objs.Length; i++) {
            if (objs[i].clientAuthorityOwner != null && objs[i].clientAuthorityOwner.connectionId == conn.connectionId) {
                NetworkServer.Destroy(objs[i].gameObject);
            }
        }
    }

    void OnSpawnRequested (NetworkMessage msg) {
        Debug.Log("Spawn request received!");
        var strMsg = msg.ReadMessage<StringMessage>();
        if (strMsg != null) {
            ulong steamId;
            if (ulong.TryParse(strMsg.value, out steamId)) {
                var conn = GetClient(new CSteamID(steamId));
                if (conn != null) {
                    if (SpawnPlayer(conn)) {
                        Debug.Log("Spawned player");
                        return;
                    }
                }
            }
        }

        Debug.LogError("Failed to spawn player!");
    }

    void OnP2PSessionRequested (P2PSessionRequest_t pCallback) {
        Debug.Log("P2P session request received");

        var member = pCallback.m_steamIDRemote;

        if (NetworkServer.active && SteamNetworkManager.Instance.IsMemberInSteamLobby(member)) {
            Debug.Log("P2P connection accepted");
            SteamNetworking.AcceptP2PSessionWithUser(member);
            CreateP2PConnectionWithPeer(member);
        }
    }

    public void CreateP2PConnectionWithPeer (CSteamID peer) {
        Debug.Log("Sending P2P acceptance message and creating remote client ref for UNET Server...");
        SteamNetworking.SendP2PPacket(peer, null, 0, EP2PSend.k_EP2PSendReliable);
        var newConn = new SteamNetworkConnection(peer);
        newConn.ForceInitialize();
        NetworkServer.AddExternalConnection(newConn);
        AddConnection(newConn);
    }

    public bool IsHostingServer () {
        return NetworkServer.active;
    }

    public void Disconnect () {
        for (int i = 0; i < connectedClients.Count; i++) {
            NetworkServer.SetClientNotReady(connectedClients[i]);
            var steamConn = connectedClients[i] as SteamNetworkConnection;
            if (steamConn != null) {
                if (SteamManager.Initialized)
                    SteamNetworking.CloseP2PSessionWithUser(steamConn.steamId);
            }
        }

        connectedClients.Clear();

        if (NetworkServer.active)
            NetworkServer.Shutdown();

        inviteFriendOnStart = false;
    }

    public void AddConnection (NetworkConnection conn) {
        connectedClients.Add(conn);
    }

    public void RemoveConnection(CSteamID steamId) {
        var conn = GetClient(steamId);
        var steamConn = conn as SteamNetworkConnection;

        if (conn != null) {
            conn.InvokeHandlerNoData(MsgType.Disconnect);

            if (steamConn != null)
                steamConn.CloseP2PSession();

            DestroyPlayer(conn);
            connectedClients.Remove(conn);

            conn.hostId = -1;
            conn.Disconnect();
            conn.Dispose();
            conn = null;
        }
    }

    public CSteamID GetSteamIDForConnection (NetworkConnection conn) {
        if (NetworkServer.connections.Count >= 1 && conn == NetworkServer.connections[0]) {
            return SteamUser.GetSteamID();
        }

        if (myClient != null && conn == myClient.connection) {
            return SteamUser.GetSteamID();
        }

        for (int i = 0; i < connectedClients.Count; i++) {
            if (connectedClients[i] != conn) continue;

            var steamConn = connectedClients[i] as SteamNetworkConnection;
            if (steamConn != null) return steamConn.steamId;
            else Debug.LogError("Client is not a SteamNetworkConnection!");
        }

        Debug.LogError("Could not find SteamID!");
        return CSteamID.Nil;
    }

    public NetworkConnection GetClient (CSteamID steamId) {
        if (steamId.m_SteamID == SteamUser.GetSteamID().m_SteamID) {
            if (NetworkServer.active && NetworkServer.connections.Count > 0) {
                return NetworkServer.connections[0];
            }
        }

        for (int i = 0; i < connectedClients.Count; i++) {
            var steamConn = connectedClients[i] as SteamNetworkConnection;
            if (steamConn != null && steamConn.steamId.m_SteamID == steamId.m_SteamID) {
                return steamConn;
            }
        }

        Debug.Log("Client not found.");
        return null;
    }
}
