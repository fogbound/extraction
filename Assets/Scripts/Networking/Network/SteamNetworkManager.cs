﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using Steamworks;

public class SteamNetworkManager : MonoBehaviour {
    public const int MAX_USERS = 4;
    public const string GAME_ID = "extraction-p2p";

    public enum SessionConnectionState {
        UNDEFINED,
        CONNECTING,
        CANCELLED,
        CONNECTED,
        FAILED,
        DISCONNECTING,
        DISCONNECTED
    }

    public static SteamNetworkManager Instance;

    [SerializeField] private UNETServerController UNETServerController;
    public List<GameObject> networkPrefabs;

    public NetworkClient myClient;

    public SessionConnectionState lobbyConnectionState { get; private set; }
    [HideInInspector] public CSteamID steamLobbyId;

    private Callback<LobbyEnter_t> m_LobbyEntered;
    private Callback<GameLobbyJoinRequested_t> m_GameLobbyJoinRequested;
    private Callback<LobbyChatUpdate_t> m_LobbyChatUpdate;
    private CallResult<LobbyMatchList_t> m_LobbyMatchList;

    private static HostTopology m_hostTopology = null;
    public static HostTopology hostTopology {
        get {
            if (m_hostTopology == null) {
                ConnectionConfig config = new ConnectionConfig();
                config.AddChannel(QosType.ReliableSequenced);
                config.AddChannel(QosType.Unreliable);
                m_hostTopology = new HostTopology(config, MAX_USERS);
            }

            return m_hostTopology;
        }
    }

    public static int GetChannelCount () {
        return hostTopology.DefaultConfig.Channels.Count;
    }

    private void Start () {
        Instance = this;
        DontDestroyOnLoad(this);

        LogFilter.currentLogLevel = LogFilter.Info;

        if (SteamManager.Initialized) {
            m_LobbyEntered = Callback<LobbyEnter_t>.Create(OnLobbyEntered);
            m_GameLobbyJoinRequested = Callback<GameLobbyJoinRequested_t>.Create(OnGameLobbyJoinRequested);
            m_LobbyChatUpdate = Callback<LobbyChatUpdate_t>.Create(OnLobbyChatUpdate);
            m_LobbyMatchList = CallResult<LobbyMatchList_t>.Create(OnLobbyMatchList);
        }

        UNETServerController.Init();

        string[] args = System.Environment.GetCommandLineArgs();
        string input = "";
        for (int i = 0; i < args.Length; i++) {
            if (args[i] == "+connect_lobby" && args.Length > i + 1) {
                input = args[i + 1];
            }
        }

        if (!string.IsNullOrEmpty(input)) {
            ulong lobbyId = 0;
            if (ulong.TryParse(input, out lobbyId)) {
                JoinLobby(new CSteamID(lobbyId));
            }
        }
    }

    private void Update () {
        if (!SteamManager.Initialized) return;
        if (!IsConnectedToUNETServer()) return;

        uint packetSize;
        int channels = GetChannelCount();

        // Read in the Steam packets
        for (int chan = 0; chan < channels; chan++) {
            while (SteamNetworking.IsP2PPacketAvailable(out packetSize, chan)) {
                byte[] data = new byte[packetSize];
                CSteamID senderId;
                if (SteamNetworking.ReadP2PPacket(data, packetSize, out packetSize, out senderId, chan)) {
                    NetworkConnection conn;
                    if (UNETServerController.IsHostingServer()) {
                        conn = UNETServerController.GetClient(senderId);
                        if (conn == null) {
                            P2PSessionState_t sessionState;
                            if (SteamNetworking.GetP2PSessionState(senderId, out sessionState) && Convert.ToBoolean(sessionState.m_bConnectionActive)) {
                                Debug.Log("P2P connection is still established; Resetting connection...");
                                SteamNetworking.CloseP2PSessionWithUser(senderId);
                                UNETServerController.CreateP2PConnectionWithPeer(senderId);
                                conn = UNETServerController.GetClient(senderId);
                            }
                        }
                    }
                    else {
                        conn = myClient.connection;
                    }

                    if (conn != null) {
                        conn.TransportReceive(data, Convert.ToInt32(packetSize), chan);
                    }
                }
            }
        }

    }

    public void RegisterNetworkPrefabs () {
        for (int i = 0; i < networkPrefabs.Count; i++) {
            ClientScene.RegisterPrefab(networkPrefabs[i]);
        }
    }

    public bool IsMemberInSteamLobby (CSteamID steamUser) {
        if (SteamManager.Initialized) {
            int numMembers = SteamMatchmaking.GetNumLobbyMembers(steamLobbyId);
            for (int i = 0; i < numMembers; i++) {
                var member = SteamMatchmaking.GetLobbyMemberByIndex(steamLobbyId, i);
                if (member.m_SteamID == steamUser.m_SteamID) {
                    return true;
                }
            }
        }

        return false;
    }

    public CSteamID GetSteamIDForConnection (NetworkConnection conn) {
        if (UNETServerController.IsHostingServer()) {
            return UNETServerController.GetSteamIDForConnection(conn);
        }
        else {
            var steamConn = myClient as SteamNetworkClient;
            if (steamConn != null) return steamConn.steamConnection.steamId;
        }

        Debug.LogError("Could not find SteamID!");
        return CSteamID.Nil;
    }

    public bool IsConnectedToUNETServer () {
        return myClient != null && myClient.connection != null && myClient.connection.isConnected;
    }

    public void Disconnect () {
        lobbyConnectionState = SessionConnectionState.DISCONNECTED;
        ClientScene.DestroyAllClientObjects();

        if (SteamManager.Initialized) SteamMatchmaking.LeaveLobby(steamLobbyId);

        if (myClient != null) {
            myClient.Disconnect();
            myClient = null;
        }

        UNETServerController.Disconnect();
        NetworkClient.ShutdownAll();

        steamLobbyId.Clear();
    }

    void OnLobbyChatUpdate (LobbyChatUpdate_t pCallback) {
        if (pCallback.m_rgfChatMemberStateChange == (uint)EChatMemberStateChange.k_EChatMemberStateChangeLeft
            && pCallback.m_ulSteamIDLobby == steamLobbyId.m_SteamID) {
            Debug.Log("A Steam user has disconnected.");
            var userId = new CSteamID(pCallback.m_ulSteamIDUserChanged);
            if (UNETServerController.IsHostingServer()) UNETServerController.RemoveConnection(userId);
            SteamNetworking.CloseP2PSessionWithUser(userId);
            Debug.Log($"{SteamFriends.GetFriendPersonaName(userId)} ({userId.ToString()}) has disconnected from the server.");
        }
    }

    void OnGameLobbyJoinRequested (GameLobbyJoinRequested_t pCallback) {
        JoinLobby(pCallback.m_steamIDLobby);
    }

    public void JoinLobby (CSteamID lobbyId) {
        if (!SteamManager.Initialized) {
            lobbyConnectionState = SessionConnectionState.FAILED;
            return;
        }

        lobbyConnectionState = SessionConnectionState.CONNECTING;
        SteamMatchmaking.JoinLobby(lobbyId);
    }

    public void InviteFriendsToLobby () {
        if (lobbyConnectionState == SessionConnectionState.CONNECTING) return;
        if (lobbyConnectionState != SessionConnectionState.CONNECTED) CreateLobbyAndInviteFriend();
        else UNETServerController.InviteFriendsToLobby();
    }

    public void CreateLobbyAndInviteFriend () {
        if (!SteamManager.Initialized) {
            lobbyConnectionState = SessionConnectionState.FAILED;
            return;
        }

        lobbyConnectionState = SessionConnectionState.CONNECTING;

        SteamMatchmaking.AddRequestLobbyListStringFilter("game", GAME_ID, ELobbyComparison.k_ELobbyComparisonEqual);
        var call = SteamMatchmaking.RequestLobbyList();
        m_LobbyMatchList.Set(call, OnLobbyMatchList);
    }

    void OnLobbyMatchList (LobbyMatchList_t pCallback, bool bIOFailure) {
        uint numLobbies = pCallback.m_nLobbiesMatching;
        if (numLobbies <= 0) {
            Debug.Log("No lobbies found; Creating new lobby...");
            UNETServerController.inviteFriendOnStart = false;
            SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypePublic, MAX_USERS);
        }
        else {
            Debug.Log("Joining lobby...");
            var lobby = SteamMatchmaking.GetLobbyByIndex(0);
            JoinLobby(lobby);
        }
    }

    void OnLobbyEntered (LobbyEnter_t pCallback) {
        if (!SteamManager.Initialized) {
            lobbyConnectionState = SessionConnectionState.FAILED;
            return;
        }

        steamLobbyId = new CSteamID(pCallback.m_ulSteamIDLobby);

        Debug.Log("Connected to Steam lobby!");
        lobbyConnectionState = SessionConnectionState.CONNECTED;

        var hostUserId = SteamMatchmaking.GetLobbyOwner(steamLobbyId);
        var localSteamId = SteamUser.GetSteamID();
        if (hostUserId.m_SteamID == localSteamId.m_SteamID) {
            SteamMatchmaking.SetLobbyData(steamLobbyId, "game", GAME_ID);
            UNETServerController.StartUNETServer();
        }
        else {
            StartCoroutine(RequestP2PConnectionWithHost());
        }
    }

    IEnumerator RequestP2PConnectionWithHost () {
        var hostUserId = SteamMatchmaking.GetLobbyOwner(steamLobbyId);
        Debug.Log("Sending packet to request P2P connection...");
        SteamNetworking.SendP2PPacket(hostUserId, null, 0, EP2PSend.k_EP2PSendReliable);

        Debug.Log("Waiting for P2P accept message...");
        uint packetSize;
        while (!SteamNetworking.IsP2PPacketAvailable(out packetSize)) {
            yield return null;
        }

        byte[] data = new byte[packetSize];

        CSteamID senderId;

        if (SteamNetworking.ReadP2PPacket(data, packetSize, out packetSize, out senderId)) {
            if (senderId.m_SteamID == hostUserId.m_SteamID) {
                Debug.Log("P2P connection established!");

                P2PSessionState_t sessionState;
                if (SteamNetworking.GetP2PSessionState(hostUserId, out sessionState)) {
                    ConnectToUNETServerForSteam(hostUserId);
                    yield break;
                }
            }
        }

        Debug.LogError("Connection failed.");
    }

    void ConnectToUNETServerForSteam (CSteamID hostSteamId) {
        Debug.Log("Connecting to UNET Server...");
        var conn = new SteamNetworkConnection(hostSteamId);
        var mySteamClient = new SteamNetworkClient(conn);
        this.myClient = mySteamClient;

        mySteamClient.RegisterHandler(MsgType.Connect, OnConnect);
        mySteamClient.SetNetworkConnectionClass<SteamNetworkConnection>();
        mySteamClient.Configure(SteamNetworkManager.hostTopology);
        mySteamClient.Connect();
    }

    void OnConnect (NetworkMessage msg) {
        Debug.Log("Connected to UNET Server!");
        myClient.UnregisterHandler(MsgType.Connect);
        RegisterNetworkPrefabs();

        var conn = myClient.connection;
        if (conn != null) {
            ClientScene.Ready(conn);
            Debug.Log("Requesting spawn...");
            myClient.Send(NetworkMessages.SpawnRequestMsg, new StringMessage(SteamUser.GetSteamID().m_SteamID.ToString()));
        }
    }
}
