﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using Steamworks;


public class SteamNetworkConnection : NetworkConnection {
    public CSteamID steamId;

    public SteamNetworkConnection () : base() {

    }

    public SteamNetworkConnection (CSteamID steamId) {
        this.steamId = steamId;
    }

    public override bool TransportSend (byte[] bytes, int numBytes, int channelId, out byte error) {
        if (steamId.m_SteamID == SteamUser.GetSteamID().m_SteamID) {
            TransportReceive(bytes, numBytes, channelId);
            error = 0;
            return true;
        }

        EP2PSend eP2PSendType = EP2PSend.k_EP2PSendReliable;

        QosType qos = SteamNetworkManager.hostTopology.DefaultConfig.Channels[channelId].QOS;
        if (qos == QosType.Unreliable || qos == QosType.UnreliableFragmented || qos == QosType.UnreliableSequenced) {
            eP2PSendType = EP2PSend.k_EP2PSendUnreliable;
        }

        if (SteamNetworking.SendP2PPacket(steamId, bytes, (uint)numBytes, eP2PSendType, channelId)) {
            error = 0;
            return true;
        }
        else {
            error = 1;
            return false;
        }
    }

    public void CloseP2PSession () {
        SteamNetworking.CloseP2PSessionWithUser(steamId);
        steamId = CSteamID.Nil;
    }
}
