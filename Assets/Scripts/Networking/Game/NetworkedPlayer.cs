﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using Steamworks;

public class NetworkedPlayer : NetworkBehaviour {
    [SyncVar] public ulong steamId;

    public override void OnStartServer () {
        base.OnStartServer();
        StartCoroutine(SetNameWhenReady());
    }

    IEnumerator SetNameWhenReady () {
        var id = GetComponent<NetworkIdentity>();
        while (id.clientAuthorityOwner == null)
            yield return null;
        steamId = SteamNetworkManager.Instance.GetSteamIDForConnection(id.clientAuthorityOwner).m_SteamID;
    }

    private void Update () {
        
    }
}
