﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;

public class DebugUI : MonoBehaviour
{
    [SerializeField] Text SteamUsername;
    [SerializeField] Text SteamID;
    [SerializeField] Text FPS;
    [SerializeField] Text Pos;
    [SerializeField] Text Rot;
    [SerializeField] Text SceneObjs;

    [SerializeField] GameObject PlayerObj;

    // FPS counters
    int m_FrameCounter = 0;
    float m_TimeCounter = 0f;
    float m_LastFR = 0f;
    public float RefreshTime = 0.5f;

    private void Update () {
        // Get FPS
        if (m_TimeCounter < RefreshTime) {
            m_TimeCounter += Time.deltaTime;
            m_FrameCounter++;
        }
        else {
            if (RefreshTime <= 0) return;
            m_LastFR = m_FrameCounter / m_TimeCounter;
            m_FrameCounter = 0;
            m_TimeCounter = 0f;
        }

        UpdateUI();
    }

    private void UpdateUI () {
        if (SteamManager.Initialized) {
            SteamUsername.text = $"Steam User: {SteamFriends.GetPersonaName()}";
            SteamID.text = $"SteamID: {SteamUser.GetSteamID().ToString()}";
        }
        FPS.text = $"FPS: {m_LastFR}";
        if (PlayerObj != null) {
            Pos.text = $"Pos: {V3ToStr(PlayerObj.transform.position)}";
            Rot.text = $"Rot: {V3ToStr(PlayerObj.transform.rotation.eulerAngles)}";
        }
        SceneObjs.text = $"# Scene Objects: {FindObjectsOfType(typeof(GameObject)).Length}";
    }

    private string V3ToStr (Vector3 vec) {
        return $"{vec.x} {vec.y} {vec.z}";
    }
}
