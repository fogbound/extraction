# Extraction
#### An open-source, team-based combat game by Fogbound.

## About Extraction
Extraction is a multiplayer team-based combat game inspired by the old SourceMod game SourceForts. Games in Extraction are split into phases -- building and combat -- during which players will build up structures and then fight to complete objectives.

## Development
Extraction contains paid content from the Unity Asset Store, and such content is not included in the repository. To download this content, please [visit our website and sign in to access the content](https://fogbound.io/login).